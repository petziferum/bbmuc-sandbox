import Vue from "vue";
import Vuetify from "vuetify/lib/framework";

Vue.use(Vuetify);

export default new Vuetify({
  theme: {
    options: {
      customProperties: true
    },
    dark: false,
    themes: {
      light: {
        primary: '#AD137C',
        //primary: '#343b29', //Flaschengrün
        //primary: '#006400',
        //primary: '#013220',

        secondary: '#DFDEDE',
        btn: '#c0c0c0',
        accent: '#ba468b',
        subheader: '#808080',
        accent2: "#63cf9a",
        error: '#c62828',
        success: '#008000',
        background: "#00a8ab"
      },
      dark: {
        primary: '#AD137C',
        secondary: '#292929',
        subheader: '#000000',
        accent: '#82B1FF',
        error: '#FF5252',
        info: '#2196F3',
        success: '#008000',
        warning: '#FFC107',
        background: "#000000"
      }
    }
  },
});
