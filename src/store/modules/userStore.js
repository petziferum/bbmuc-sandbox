import axios from "axios";
import User from "@/models/User";
import Vuetify from "@/plugins/vuetify.js";

export default {
  state: {
    user: null,
    userMode: { darkMode: null },
  },
  mutations: {
    SET_USER(state, payload) {
      state.user = payload;
    },
    SET_EDITUSER() {},
    SET_USERMODE(state, payload) {
      state.userMode.darkMode = payload;
    },
    getUserOwnedProjects() {},
    getUserJoinedProjects() {},
    setLinks() {},
  },
  actions: {
    userLogIn({ commit }) {
      let user = {
        username: "petzi",
        password: "asdf",
      };

      axios({
        method: "post",
        url: "/api/auth/signin",
        data: user,
      })
        .then((res) => {
          localStorage.setItem("token", res.data.accessToken);
          console.log("eingelogged... ", res.data);
          return res;
        })
        .then((res) => {
          console.log("lade Profil... ");
          const token = res.data.tokenType + " " + res.data.accessToken;
          axios({
            method: "get",
            url: "/api/getuser",
            headers: {
              Authorization: token,
            },
          }).then((res) => {
            console.log("Userprofile empfangen: ", res.data);
            commit("SET_USER", res.data);
            commit("SET_EDITUSER", user);
            //commit("SET_USERMODE", user.darkMode.bool);
          });
        });
    },
    getUserData({ commit, dispatch }) {
      if (localStorage.getItem("token") != null) {
        axios({
          method: "get",
          url: "/api/getuser",
          headers: {
            Authorization: "Bearer " + localStorage.getItem("token"),
          },
        }).then((res) => {
          //Lädt die Bezeichnung für Userrollen aus der DB
          let user = new User.buildUser(res.data);
          commit("SET_USER", user);
          commit("SET_EDITUSER", user);
          commit("SET_USERMODE", user.darkMode.bool);
          dispatch("getUserOwnedProjects", user.ownProjects.value); //Übergibt die ID's, der Projekte, die dem User gehören
          dispatch("getUserJoinedProjects", user.joinedProjects.value); //Übergibt die ID's, der Projekte, an denen der User angemeldet ist
          dispatch("setLinks");
          Vuetify.framework.theme.dark = user.darkMode.bool;
        });
      }
    },
    userLogout({ commit }) {
      commit("SET_USER", null);

      localStorage.clear();
      console.info("Ausgelogged");
    },
  },
  getters: {
    getUser: (state) => {
      return state.user;
    },
  },
};
