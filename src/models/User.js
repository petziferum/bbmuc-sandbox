import Images from "@/store/data/images";
export default class User {
  id = {};
  firstname = {};
  surname = {};
  userDepartment = {};
  email = {};
  darkMode = {};
  joinedProjects = {};
  ownProjects = {};
  roles = [];
  userImage = {};
  userOffice = {};
  username = {};

  constructor() {}

  static buildUser(obj) {
    let u = new User();
    u.id = { text: "UserID", value: obj.id, icon: Images.icons.userId };
    u.username = {
      text: "Username",
      value: obj.username,
      icon: Images.icons.owner,
    };
    u.firstname = {
      text: "Vorname",
      value: obj.firstname,
      icon: Images.icons.owner,
    };
    u.surname = {
      text: "Nachname",
      value: obj.surname,
      icon: Images.icons.owner,
    };
    u.userDepartment = {
      text: "Eigenbetrieb/ Referat",
      value: obj.userDepartment,
      icon: Images.icons.department,
    };
    u.email = { text: "Email", value: obj.email, icon: Images.icons.email };
    u.darkMode = {
      text: "Colormode",
      value: obj.darkMode ? "Darkside" : "Light",
      bool: obj.darkMode,
      icon: Images.icons.theme,
    };
    u.joinedProjects = {
      text: "aktive Projekte, an denen du teilnimmst",
      value: obj.joinedProjects,
    };
    u.ownProjects = {
      text: "Projekte, die du erstellt hast",
      value: obj.ownProjects,
    };
    u.userImage = { text: "Profilbild", value: obj.userImage };
    u.userOffice = {
      text: "Abteilung",
      value: obj.userOffice,
      icon: Images.icons.unit,
    };
    u.roles = {
      text: obj.roles.length == 1 ? "Rolle" : "Rollen",
      value: [],
      icon: Images.icons.role,
    };
    obj.roles.forEach((el) => {
      u.roles.value.push(
        el.name.split("_")[1][0].toUpperCase() +
          el.name.split("_")[1].slice(1).toLowerCase()
      );
    });

    return u;
  }

  get id() {
    return this.id;
  }
  set id(value) {
    this.id = value;
  }
  get firstname() {
    return this.firstname;
  }
  set firstname(value) {
    this.firstname = value;
  }
  get surname() {
    return this.surname;
  }
  set surname(value) {
    this.surname = value;
  }
  get userDepartment() {
    return this.userDepartment;
  }
  set userDepartment(value) {
    this.userDepartment = value;
  }
  get email() {
    return this.email;
  }
  set email(value) {
    this.email = value;
  }
  get darkMode() {
    return this.darkMode;
  }
  set darkMode(value) {
    this.darkMode = value;
  }
  get joinedProjects() {
    return this.joinedProjects;
  }
  set joinedProjects(value) {
    this.joinedProjects = value;
  }
  get ownProjects() {
    return this.ownProjects;
  }
  set ownProjects(value) {
    this.ownProjects = value;
  }
  get roles() {
    return this.roles;
  }
  set roles(value) {
    this.roles = value;
  }
  get userImage() {
    return this.userImage;
  }
  set userImage(value) {
    this.userImage = value;
  }
  get userOffice() {
    return this.userOffice;
  }
  set userOffice(value) {
    this.userOffice = value;
  }
  get username() {
    return this.username;
  }
  set username(value) {
    this.username = value;
  }
}
